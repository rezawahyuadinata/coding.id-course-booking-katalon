import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'com.tools.keyword.LoginAuth.performLogin'()

WebUI.click(findTestObject('Object Repository/Event/Page_Be a Profressional Talent with Coding.ID/a_Events'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Event/Page_Online event bersertifikat dari prakti_f42b96/h2_Events'))

WebUI.scrollToElement(findTestObject('Object Repository/Event/Page_Online event bersertifikat dari prakti_f42b96/li_Day 4 Workshop                          _0cca4f'), 
    3)

WebUI.verifyElementText(findTestObject('Object Repository/Event/Page_Online event bersertifikat dari prakti_f42b96/h2_Events'), 
    'Events')

WebUI.click(findTestObject('Object Repository/Event/Page_Online event bersertifikat dari prakti_f42b96/div_Day 4 Workshop                         _31d43a'))

WebUI.navigateToUrl('https://demo-app.online/event/day-4-workshop-flya6jmot1vq')

WebUI.scrollToElement(findTestObject('Object Repository/Event/Page_Day 4 Workshop - Ziyad/div_Harga Kelas                            _67e05a'), 
    2)

WebUI.click(findTestObject('Object Repository/Event/Page_Day 4 Workshop - Ziyad/a_Beli Tiket'))

WebUI.verifyElementVisible(findTestObject('Object Repository/Event/Page_Day 4 Workshop - Ziyad/h3_Event already on Cart'))

WebUI.verifyElementText(findTestObject('Object Repository/Event/Page_Day 4 Workshop - Ziyad/h3_Event already on Cart'), 
    'add to cart success')

WebUI.closeBrowser()

