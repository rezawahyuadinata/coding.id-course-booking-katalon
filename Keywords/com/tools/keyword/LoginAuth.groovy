package com.tools.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class LoginAuth {
	@Keyword
	public static void performLogin() {
		WebUI.openBrowser('')
		WebUI.deleteAllCookies()
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://demo-app.online/')
		WebUI.click(findTestObject('Object Repository/Login/Page_Be a Profressional Talent with Coding.ID/a_Masuk'))
		WebUI.setText(findTestObject('Object Repository/Login/Page_Masuk untuk dapatkan akses di Coding.ID/input_Email_email'),
				'adinatarezawahyu@gmail.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Login/Page_Masuk untuk dapatkan akses di Coding.ID/input_Kata                                 _98da12'),
				'Gz08DaJU9NXyFmL46LbsMw==')
		WebUI.click(findTestObject('Object Repository/Login/Page_Masuk untuk dapatkan akses di Coding.ID/button_Login'))
		WebUI.verifyElementText(findTestObject('Object Repository/Login/Page_Be a Profressional Talent with Coding.ID/h5_Bootcamp untuk semua,            dengan _5cbf2e'),
				'Bootcamp untuk semua, dengan background IT maupun Non-IT. Pilih programnya dan dapatkan jaminan kerja.', FailureHandling.CONTINUE_ON_FAILURE)
	}
}
