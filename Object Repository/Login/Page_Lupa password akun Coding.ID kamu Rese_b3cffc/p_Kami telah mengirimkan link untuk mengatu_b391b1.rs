<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Kami telah mengirimkan link untuk mengatu_b391b1</name>
   <tag></tag>
   <elementGuidId>aa18fd21-cfb1-4f8b-b0e3-f91c1bc26b23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.text-muted</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa Kata Sandi'])[1]/following::p[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>b979f944-3e36-47c5-9991-57e1841ed277</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-muted</value>
      <webElementGuid>a3a01b4c-7e1a-4f1a-ac35-b308f23749cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        Kami telah mengirimkan link untuk mengatur ulang kata sandi anda.Silahkan periksa email anda
                    </value>
      <webElementGuid>a3e441b7-79c1-4155-8df2-d3ed5491dc1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;___class_+?3___&quot;]/p[@class=&quot;text-muted&quot;]</value>
      <webElementGuid>83a4e7e2-eb0f-49a9-b52d-976003114d64</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa Kata Sandi'])[1]/following::p[1]</value>
      <webElementGuid>892878e8-ec04-4788-8cae-8913a6350847</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kirim link'])[1]/preceding::p[1]</value>
      <webElementGuid>75d9ca20-19b0-4551-a521-47641b31cca4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::p[2]</value>
      <webElementGuid>c3131087-061d-4f33-ba08-b232f9739aa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kami telah mengirimkan link untuk mengatur ulang kata sandi anda.']/parent::*</value>
      <webElementGuid>895b5bd8-a5b0-48bc-b035-a280d224e337</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div[2]/p</value>
      <webElementGuid>baf8f666-ec81-4fc1-a9f0-43a3bbb26fff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = '
                        Kami telah mengirimkan link untuk mengatur ulang kata sandi anda.Silahkan periksa email anda
                    ' or . = '
                        Kami telah mengirimkan link untuk mengatur ulang kata sandi anda.Silahkan periksa email anda
                    ')]</value>
      <webElementGuid>8fc6d85a-f9f0-4134-8dbe-11ecb046aa97</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
