<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Tutup</name>
   <tag></tag>
   <elementGuidId>0ce0d54f-9a26-4331-8a96-7eb2b06e0290</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Modal_Success']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2fbcb19c-79e2-4bae-84b4-4969f2c8f239</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>6a81c098-9ff8-459e-86ac-bf69a7f71fba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
             Tutup 
            
            

        </value>
      <webElementGuid>5222e00b-3374-4878-89a7-c41bdb6bb4d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Modal_Success&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>92e7484c-984e-4249-8c34-6be289083189</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div</value>
      <webElementGuid>4ad2a2ae-ce8d-4132-b49e-630c4eee1fff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi Pembicara di Event Coding.id'])[1]/following::div[3]</value>
      <webElementGuid>f5d8f3fb-0c9b-491a-92d7-65c1a104ca8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event already on Cart'])[1]/preceding::div[1]</value>
      <webElementGuid>1961996c-cefa-4ea2-a516-2c30b8ff3133</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div</value>
      <webElementGuid>8a35037b-153e-4848-9d99-d35850b82c02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            
             Tutup 
            
            

        ' or . = '
            
             Tutup 
            
            

        ')]</value>
      <webElementGuid>7191b7e4-aa83-43b3-a30e-ab0fd5d52914</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
