<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Harga Kelas                            _67e05a</name>
   <tag></tag>
   <elementGuidId>a2750d75-a634-486e-8ced-f121ad9db618</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>13d9c344-36fb-4466-b6aa-c0b7a5237633</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-event-options</value>
      <webElementGuid>ac2ec4b7-347f-4de7-936c-c2e185a851c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 15  Menit  48 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                </value>
      <webElementGuid>38ac061b-9c32-434b-97b4-c3d962d56025</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[@class=&quot;wm-sticky&quot;]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/aside[@class=&quot;col-md-4&quot;]/div[@class=&quot;wm-event-options&quot;]</value>
      <webElementGuid>97f174c7-0036-42ff-ac46-b1bd696b76ed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::div[1]</value>
      <webElementGuid>492b7042-4395-42ca-ae32-38bef3db0cb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[1]/following::div[1]</value>
      <webElementGuid>15c5a083-5c50-4494-9f3b-2dd3759f60de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]</value>
      <webElementGuid>6e4e5e9c-9f4b-416a-bca4-bc86613cff25</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 15  Menit  48 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                ' or . = '
                    
                        
                        
                            Harga Kelas
                                                                                                Rp.
                                        500.000
                                                                                                                        Rp
                                    85.000
                            
                        

                        
                            
                            Tanggal:
                            25 November 2023
                            
                            
                        
                        
                            
                            Jam:
                            
                            19:30 
                                WIB 
                        


                        
                            
                            Lokasi


                            Zoom

                        

                                                
                            
                            Waktu Pendaftaran Tersisa
                            
                            79 Hari 18 Jam 15  Menit  48 Detik 
                            

                        
                        
                                                                                                
                                                                                                                        Beli Tiket
                                        
                                                                    

                                                        

                            
                        


                    
                ')]</value>
      <webElementGuid>70196956-8274-4f27-ae9e-e9ad70d03287</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
